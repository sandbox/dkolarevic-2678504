
VideoJS Youtube
-----------------

Play videos from Youtube and add subtitles and poster(preview image) to them.

 Installation
 ------

  * Be sure following modules are installed and enabled:

    -field_collection
    -libraries

  * Like any other modules, put "vjs_youtube" module folder in "sites/all/modules".

  * Download "video-js youtube" library and place it in "sites/all/libraries/vjs-youtube".

  * Go to "Admin -> Modules" and enable "VideoJS Youtube" module.

 Usage
 ------

  * Once installation has done, "VideoJS Youtube" content type is provided. 

  * To add new content go to "Admin -> Content -> Add content -> VideoJS Youtube".

  * Enter title, a valid Youtube link in "Youtube video link" field and add one or more subtitle files to it using "Video subtitle file" and "Video 
    subtitle language" fields, and add poster using "Video poster" field.


